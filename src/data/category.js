const categoryData=[
	{
		id: '1',
		name: 'GREEN',
		param:'fiction',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.'
	},
	{
		id: '2',
		name: 'BLACK',
		param:'nonFiction',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.'
	},
	{
		id: '3',
		name: 'FRUIT',
		param:'fiction',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.'
	},
	{
		id: '4',
		name: 'HERBAL',
		param:'nonFiction',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.'
	}

]
export default categoryData;