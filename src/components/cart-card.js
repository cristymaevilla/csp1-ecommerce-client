import {useState, useContext } from 'react';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types'
import { Button, Card, Row, Col }from 'react-bootstrap';
import Image from 'react-bootstrap/Image'
import productImage from '../images/product.png'; 
import {Link, Redirect, useHistory} from 'react-router-dom';
import UserContext from '../user-context';
import { FaTimes } from "react-icons/fa";



export default function CartCard({itemProp}){

const{id, name, addedOn, quantity, stocks, price, subTotal}=itemProp;

const {user, setUser}=useContext(UserContext);
	const[newQuantity, setNewQuantity]= useState(quantity);
	const[newSubTotal, setNewSubTotal]= useState(subTotal);
	const[isDeleted, setIsDeleted]= useState(false);
	const[cartItems, setCartItems]= useState([]);
	const[item, setItem]= useState([]);


	function increment(){
		if(newQuantity === 20){setNewQuantity(20); setNewSubTotal(newSubTotal); ;  }
		// if(newQuantity > stocks){
		// 	Swal.fire({
		// 	          title: `Good gracious, ${stocks} stocks left!`,
		// 	          icon: 'warning',
		// 	          text: 'Please change the quantity of your item.'   
		// 	      });
		// 	      setNewQuantity(newQuantity);
		// 	      computeSubtotal()
		// }
		else{setNewQuantity(newQuantity +1); setNewSubTotal(newSubTotal+itemProp.price); }
	}
	function decrement(){
		if(newQuantity === 1){setNewQuantity(1); setNewSubTotal(newSubTotal);  }
		else{setNewQuantity(newQuantity-1); setNewSubTotal(newSubTotal-itemProp.price);  }}

		    
	function retrieveItem(){
		console.log(itemProp._id);
			fetch(`http://localhost:4000/cart/${itemProp.id}`, {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		    .then (res=> res.json())
		    .then(data =>{
		    	if(data.lenght !== 1){setIsDeleted(true); console.log(data)}
		    	else{setItem(data);}
		    	// data.splice(data.indexOf(itemProp._id));
		    	// setIsDeleted(true);
		    })

	}


/*	function retrieveCartItems(){
			fetch('http://localhost:4000/cart', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		    .then (res=> res.json())
		    .then(items=>{
		    	console.log(items);
		    	console.log(itemProp._id);
		    	// setCartItems(data);
		    	// items.splice(items.indexOf(itemProp._id));
		    	for(var i = 0; i < items.length; i++) {
		    	    if(items[i]._id === itemProp._id) {
		    	       
		    	        items.splice(i,1);
		    	        break;
		    	    }
		    	}
		    	// var index = items.map(function(x) {return x._id; }).indexOf(itemProp._id);
		    	// console.log(index);
		    	// items.splice(items.indexOf(itemProp._id));

		    	// setIsDeleted(true);
		    })
		}*/
//  function remove (){
// 	console.log(itemProp._id);
// // 	fetch(`http://localhost:4000/cart/${itemProp._id}`, {
// // 	method: "DELETE"
// // });
// // }
// 		            fetch('http://localhost:4000/cart/id', {
// 		            	method:"DELETE",
// 		                headers: {
// 		                    Authorization: `Bearer ${ localStorage.token }`
// 		                }
// 		                })
// 		            .then(res => res.json())
// 		            .then(data => {
// 		                console.log(data);
// 		                console.log(itemProp._id);
// 		            })}
		        
function remove(){
	console.log(itemProp._id);
	setIsDeleted(true);
    const requestOptions = {
        method: 'DELETE',
        headers: { 
            'Authorization': `Bearer ${ localStorage.token }`
         
        }
    };
    fetch(`http://localhost:4000/cart/${itemProp._id}/delete`, requestOptions)
        .then((res) => console.log(res));
}

    // fetch('https://jsonplaceholder.typicode.com/posts/1', { method: 'DELETE' })
    //         .then(() => this.setState({ status: 'Delete successful' }));
    // }
 function getId(){console.log(itemProp.quantity)}
function itemUpdate(){
			console.log(itemProp._id);
			Swal.fire({title:'Item updated',
						icon: "success"})
			fetch(`http://localhost:4000/cart/${itemProp._id}/update`, {
			    method: 'PUT',
			     headers: {
			         'Content-Type': 'application/json',
			         Authorization: `Bearer ${ localStorage.token }`
			               },
			   body: JSON.stringify({
			         quantity: newQuantity,
			         price: price,
			         subTotal:newSubTotal
			    })
			    })
			.then (res=> res.json()).then(data =>{console.log(data);
				
			})
		}

		return( 
		(isDeleted) ? null :
		<Col className=" col-12 m-o p-0 mb-5 mb-sm-2">
			<Card className="p-0 m-0 justify-content-around col-12">
			<Card.Body className="p-0 d-sm-flex justify-content-around col-12">

				<div className="col-sm-3 container-fluid m-0 p-0">
					<Image  src={productImage} fluid className="img-fluid m-0 p-0 float-center"/>		
				</div>
				<div className="col-sm-6 container-fluid d-flex-column pt-0 pt-sm-4 text-center text-sm-left pl-0 pr-0 ml-0 mr-0"> 
					<h3>{name}</h3>
					<p className="pt-0 pb-0 mb-0">Added on {addedOn}</p>
					<p className="pt-0 pb-0 mb-0">Stocks {stocks}</p>
					<h5 className="pt-0 pb-0 mb-1">Price {price}</h5>
				</div>
				<div className="col-sm-3 container-fluid d-flex-column text-center mt-sm-0 mt-md-4 p-0 m-0 pt-sm-3 pt-lg-4"> 
					<div>
						<span className="item2 pl-2 pr-2 text-center" onClick={decrement}>-</span>
						<span className="item1  pl-2 pr-2 text-center ">{newQuantity}</span>
						<span className="item2 pl-2 pr-2 text-center" onClick={increment}>+</span>
					</div>
					<h4>Subtotal <b>{newSubTotal}</b></h4>
					<Button variant="primary col-sm-8 col-12 mb-sm-1" onClick={remove}><FaTimes /></Button>	
					<Button variant="primary col-sm-8 col-12"onClick={itemUpdate}>Update</Button>				
				</div>
				
			</Card.Body>
			</Card>
				
		</Col>	
		
				
			)
	}

	// checks the validity of proptypes
	CartCard.propTypes = {
		// shape()-if prop object conforms with the specific shape
		product:PropTypes.shape({
			name:PropTypes.string.isRequired,
			addedOn:PropTypes.instanceOf(Date).isRequired,
			price: PropTypes.number.isRequired,
			quantity: PropTypes.number.isRequired,
			subTotal:PropTypes.string.isRequired
		
		})
	}