import PropTypes from 'prop-types'
import { MdOutlineDeliveryDining, MdTaskAlt } from "react-icons/md";
import {Link} from 'react-router-dom';
import {useState, useContext } from 'react';
import UserContext from '../user-context';
import { Button, Card, Row, Col }from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function OrderCard({orderProp}){
	
	const {user, setUser}=useContext(UserContext);
	const{id, purchasedOn, shippingFee, subTotal, status, totalAmount, paymentMethod}=orderProp;
	const[isDelivered, setIsDelivered]= useState(false);
	function update(){
			  }

function update(){
			console.log(orderProp._id);
			fetch(`http://localhost:4000/orders/${orderProp._id}/status`, {
			    method: 'PUT',
			     headers: {
			         'Content-Type': 'application/json',
			         Authorization: `Bearer ${ localStorage.token }`
			               },
			   body: JSON.stringify({
			         status: "delivered",
			    })
			    })
			.then (res=> res.json()).then(data =>{
				if(data !== false ){
					setIsDelivered(true);
					const Toast = Swal.mixin({
					  toast: true,
					  position: 'top-end',
					  showConfirmButton: false,
					  timer: 3000,
					  timerProgressBar: true,
					  didOpen: (toast) => {
					    toast.addEventListener('mouseenter', Swal.stopTimer)
					    toast.addEventListener('mouseleave', Swal.resumeTimer)
					  }
					})

					Toast.fire({
					  icon: 'success',
					  title: 'Updated in successfully'
					})
				}
				
			})
		}

	
	return( 
			(user.isAdmin) ?

				<Row className="mt-2 mb-2 p-0 col-12 d-flex-column justify-content-center">
				<Card className=" p-0 col-12 shadow1">
				  <Card.Header className=" l-blue t-blue"><em>Order Id : {orderProp._id}</em></Card.Header>
				  <Card.Body className=" pl-2 pr-2 ml-2 mr-2 d-md-flex">
				  <div className=" col-md-6 text-left"> 
				  	<h5>Purchased on: {purchasedOn}</h5>
				    <h5>Shipping Fee: {shippingFee}</h5>
				    <h5>Payment Method : {paymentMethod}</h5>
				  </div>
				  <div className=" col-md-4 text-left"> 
				    <h5>SubTotal: {subTotal}</h5>
				    <h5>Total Amount: {totalAmount}</h5>
				   </div>
				   <div>
				   	{isDelivered ?
				   		<p  className=" text-center mb-0 t-blue">STATUS: {status} <MdTaskAlt/>  </p >
				   	  :
				   	  	<p className="text-center mb-0 t-blue" >STATUS: {status} <MdOutlineDeliveryDining/></p >
				   	   }
				   		
				   		 <span onClick={update} className="add-button float-right col-12 text-center align-middle p-1 mt-4 ml-2">
				   		 		<span>DELIVERED </span>		
				   		 </span>
				   	
				   </div>
				
				
				  </Card.Body>

				</Card>
				</Row>

			:
				<Row className="mt-2 mb-2 p-0 col-12 d-flex-column justify-content-center">
				<Card className=" p-0 col-12 shadow1">
				  <Card.Header className=" l-blue t-blue"><em>Order Id : {orderProp._id}</em></Card.Header>
				  <Card.Body className=" pl-2 pr-2 ml-2 mr-2 d-md-flex">
				  <div className=" col-md-6 text-left"> 
				  	<h5>Purchased on: {purchasedOn}</h5>
				    <h5>Shipping Fee: {shippingFee}</h5>
				    <h5>Payment Method : {paymentMethod}</h5>
				  </div>
				  <div className=" col-md-4 text-left"> 
				    <h5>SubTotal: {subTotal}</h5>
				    <h5>Total Amount: {totalAmount}</h5>
				   </div>
				   
				   <div className=" text-center t-blue">
				           STATUS: {status}
				    </div>
				
				  </Card.Body>

				</Card>
				</Row>

				
			)
	}


		OrderCard.propTypes = {

		order:PropTypes.shape({
			purchasedOn:PropTypes.string.isRequired,
			id:PropTypes.string.isRequired,
			paymentMethod: PropTypes.string.isRequired,
			shippingFee: PropTypes.number.isRequired,
			subTotal: PropTypes.number.isRequired,
			totalAmount: PropTypes.number.isRequired,
			status: PropTypes.string.isRequired
		
		})
	}