import {Fragment, useEffect, useState, useContext} from 'react';

import { Row, Col, Card, Button, Form, FormControl, NavDropdown }from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import OrderCard from '../components/order-card'
import UserContext from '../user-context';
import {Route, Link, NavLink, Redirect} from 'react-router-dom';
import { BsEmojiWink, BsClipboardCheck, BsChatSquareDots} from "react-icons/bs";



export default function Order(){
	const{user} =useContext(UserContext);
	const[isEmpty, setIsEmpty]= useState(false);
	const [orders, setOrders]=useState([]);
	useEffect(()=>{
		

		console.log(localStorage.token)
		
	fetch('http://localhost:4000/orders/users/orders', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);

			if (data !== '' || data !== false){
				setOrders(data.map(order =>{
						return(
							<OrderCard key= {order.id}orderProp ={order}/>
							)
						})
					);				
			}
			else{setIsEmpty(true)}
			
		})
	}, [])


	
	return(
	<Col className=" p-0 m-0 vh-100 mb-0 pb-0">
		<div className=" p-0 m-0  ml-2 mr-2  mb-2 nav-dashboard fixed-bottom ">
			<div className=" p-0 m-0 pt-1 pb-1 mt-5 d-sm-flex justify-content-around blue  rounded">
				<div className="col-sm-6 pl-1 pl-sm-3 pl-xl-4 hide">
					<h3 className="oblique">Hi there</h3>
					<h1 className="name">ALEXANDER</h1>
				</div>
				<div className="col-sm-6 d-flex justify-content-around align-middle text-center">
					<div className="">
						<div className="user-icons container-fluid p-0 m-0"><BsEmojiWink/></div>
						<div className="texthover">PROFILE</div>
					</div>
					<div className="">
						<div className="user-icons container-fluid p-0 m-0"> <BsClipboardCheck/></div>
						<div className="texthover">ORDERS</div>
					</div>
					<div className="">
						<div className="user-icons container-fluid p-0 m-0"><BsChatSquareDots/></div>
						<div className="texthover">REVIEWS</div>
					</div>
				</div>
			</div>
		</div>
		<div className="col-12 m-0 p-0 d-md-flex pt-2 mb-0 pb-0 vh-100 align-items-center">
			<div className="col-12 col-md-6 m-0 p-0 d-sm-flex pt-md-2 pb-md-5 pb-0 mt-5 vh-50 vh-md-100 ">
				<div className="col-12 m-0 p-0 blue mb-0 pt-0 pt-md-5 pb-md-5  shadow-1 rounded">
				sdsddgfdgfd
				</div>
			</div>
			<div className="col-12 col-md-6 m-0 p-0 pb-5 mt-1  container-right pl-2 pr-2 vh-50 vh-md-100">
				<h2 className="text-center">My Orders</h2>
				<Fragment className="mb-5 mb-sm-0">
					{orders}	
				</Fragment>
			</div >
		</div>
				
	</Col>
				
	)
}
