import { useContext, useEffect } from 'react'
import { Redirect } from 'react-router-dom';
import UserContext from '../user-context';

export default function Logout(){
	const { unsetUser, setUser }= useContext(UserContext);

	unsetUser();

	useEffect(() => {
		setUser({id: null});
	})

	// Redirect back to login
	return(
		<Redirect to="/login" />
	)
}
