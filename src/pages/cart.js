import { TiShoppingCart } from "react-icons/ti";
import {Fragment, useEffect, useState, useContext} from 'react';

import { Row, Col, Card, Button, Form, FormControl, NavDropdown }from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import CartCard from '../components/cart-card'
import UserContext from '../user-context';
import {Route, Link, NavLink, Redirect} from 'react-router-dom';
import { IconName } from "react-icons/bs";
import Swal from 'sweetalert2';


export default function Items(){
	

	// State that will be used to store the curses retrieved from the database
	let info=["cash on delivery", "payment center", "online payment"]
	const [items, setItems]=useState([]);
	const{user} =useContext(UserContext);
	// const[items, setItems]= useState([]);
	const[all, setAll]=useState([]);
	const[total, setTotal]= useState("");
	const[orderTotal, setOrderTotal]= useState(0);
	const[shippingFee, setShippingFee]= useState(0);
	const[isEmpty, setIsEmpty]= useState(false);
	const[itemList, setItemList]= useState([]);
	const[paymentMethod, setPaymentMethod]= useState("");
	const[isCheckedOut, setIsCheckedOut]= useState(false);
	const[appear, setAppear]= useState(false);
	useEffect(()=>{
		
		console.log(user.id);
		console.log(`useraccess ${user.access}`);
		console.log(localStorage.token)
		
	fetch('http://localhost:4000/cart', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);

			if (data !== '' || data !== false){
				setItems(data.map(item =>{
						return(
							// key is to check the Ids in courseData
							<CartCard key= {item.id}itemProp ={item}/>
							)
						})
					);
					let x =0;
					data.forEach(item=>{x+= item.subTotal});
					setTotal(x)	
					let fee=  (x > 300 )? 0 : 150;
					let overallTotal= x+fee;
					setShippingFee(fee);
					setOrderTotal(overallTotal);
					console.log(orderTotal)
					// console.log(x)


				
			}
			else{setIsEmpty(true)}
			
		})
	}, [])

function seeOrderTotal(e){
	e.preventDefault()
	 getTotal();
	 Swal.fire({
				text:`Subtotal ${total} | Shipping Fee ${shippingFee}`,
				title: `TOTAL Php ${orderTotal} `,
	
					})
	}

function  checkOutOrder(e) {
	 	e.preventDefault();
	    retrieveData();
	    console.log(itemList.length);
	    console.log(paymentMethod);
	    
	    		fetch('http://localhost:4000/orders/checkout', {
	    		    method: "POST",
	    		    headers: {
	    		        'Content-Type': 'application/json',
	    		        Authorization: `Bearer ${ localStorage.token }`
	    		    },
	    		    body: JSON.stringify({
	    		        userId: user.id,
	    		        items: itemList,
	    		        subTotal: total,
	    		        shippingFee:shippingFee,
	    		        totalAmount:orderTotal,
	    		        paymentMethod: paymentMethod
	    		    })
	    		})
	    		.then(res => res.json())
	    		.then(data => {console.log(data);

	    		    if(data === true){
	    		        setShippingFee('');
	    		        Swal.fire({
	    		            title: 'CheckOut successful',
	    		            icon: 'success',
	    		            text: 'Thank your for ordering!'
	    		        }); setIsEmpty(true);setAppear(false);
	    		        // history.push("/login");
	    		    } else {

	    		       Swal.fire({
	    		            title: "I beg your pardon, something is wrong",
	    		            // icon: 'error',
	    		            text: 'Please try again.'   
	    		        });

	    		    };

	    		})

	    	
	 }

function retrieveData(){
	fetch('http://localhost:4000/cart', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);

			if (data !== '' || data !== false){
				let arr=[];
				for(let i=0; i< data.length; i++){
					arr.push({name:data[i].name, quantity: data[i].quantity, _id:data[i]._id})
				}setItemList(arr);
		
					let x =0;
					data.forEach(item=>{x+= item.subTotal});
					setTotal(x)	
					let fee=  (x > 300 )? 0 : 150;
					let overallTotal= x+fee;
					setShippingFee(fee);
					setOrderTotal(overallTotal);
				
					// console.log(itemList);
			}else{setIsEmpty(true)}
						
		})
}
	    



	

	
function letAppear(){ setAppear(true)}



function cod(){ setPaymentMethod('cash on delivery')
	console.log(paymentMethod);}
function op(){ setPaymentMethod('online payment')
	console.log(paymentMethod);}
function getTotal(){
	fetch('http://localhost:4000/cart', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
			.then (res=> res.json())
			.then(data =>{
				if(data !== "" || data !== undefined){
						let x =0;
					data.forEach(item=>{x+= item.subTotal});
					setTotal(x)	
					let fee=  (x > 300 )? 0 : 150;
					let overallTotal= x+fee;
					setShippingFee(fee);
					setOrderTotal(overallTotal);
					console.log(orderTotal)
					// console.log(x)
				}
					})
}

// views all  the products
	return(
		(user.id === null) ?
		 <Redirect to="/login" />
		 :
		 <Col className="pt-5 mt-2 mr-0 ml-0 pl-0 pr-0">
		{isEmpty ?
		<Col className="pt-5 mt-4 mr-0 ml-0 pl-0 pr-0 ">
		
			<h1 className="text-center">Oh  my! Your cart is empty.</h1>
			<h5 className="text-center">Looks like you haven't add anything to your cart yet.</h5>
			<div className="justify-content-center d-flex mt-5 " >
			<Button as={Link} to="/products" exact variant="primary" className="center" >SHOP NOW</Button>
			
			</div>
		</Col>
		:
		<Col className="mb-5 pb-5">
			<h1 className="text-center">My Cart</h1>
				<Fragment className="mb-5 mb-sm-0">
					{items}	
			
				</Fragment>
			<div className="fixed-bottom">
			<div className=" col-12 d-sm-flex justify-content-arround blue text-center align-middle">
				<h3 className="col-12 col-sm-6 d-sm-block d-none mb-0"><em>Free Shipping on orders above 300</em></h3>
				<p className="col-12 col-sm-6 d-sm-none d-block mb-0"><em>Free Shipping on orders above 300</em></p>
				<div className=" d-sm-flex col-12 col-sm-6 justify-content-arround align-middle">
					<h4 className="col-12 col-sm-6 pl-2 pt-3 pt-md-0 texthover" onClick={seeOrderTotal}><b>SEE TOTAL</b></h4> 
					
						<h4 className=" align-middle col-12 col-sm-6 texthover pt-3 pt-md-0" onClick={letAppear}>Check Out</h4>
						
						
					
					
					
				</div>
	
			</div>
				{appear? 
				<div className="text-center col-24 mt-4 mb-4 ">
					<h5 className="texthover" onClick={cod} >Cash On Delivery</h5>
					<h5 className="texthover" onClick={op}>Online Payment</h5>
					<p><b>Choose Payment Method: </b> {paymentMethod}</p>
					{(paymentMethod !== "") ?
					<Button variant="primary" onClick={checkOutOrder}>Place Order</Button>
					:
					<Button variant="primary" disabled >Place Order</Button>
					}		
				</div>
				: null}
			</div>
		</Col>
		}
		</Col>


		

		
		)
}