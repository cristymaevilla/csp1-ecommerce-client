import {useState, useEffect, useContext } from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../user-context';
import Image from 'react-bootstrap/Image';
import productImage from '../images/product.png';
import Swal from 'sweetalert2';
import { BiLeftArrowAlt } from "react-icons/bi";
import { FaCheck } from 'react-icons/fa';
import { NavLink } from 'react-router-dom';

// import ProductContext from '../product-context';

 export default	function ProductView(productProp){
 		// const{_id, image, name, price, description}=productProp;
 	
// const{product} =useContext(ProductContext);
		const {user, setUser}=useContext(UserContext);
		const productId = productProp.match.params.productId
		const[product, setProduct]= useState("");	
		const[items, setItems]= useState(0);
		const[review, setReview]= useState("no reviews");
		const[averageRating, setAverageRating]= useState(0);
		const[isAdded, setIsAdded]= useState(false);


		// const[product, setProduct]= useState("");
		useEffect(()=>{

			fetch(`http://localhost:4000/products/${productId}`)
			.then (res=> res.json())
			.then(data =>{
				console.log(data);
				setReview(data.Reviews);
				setProduct(data.product);
				setAverageRating(data.averageRating);
		
			})
		}, [])

		
		console.log(user.id);

		return	(
				<Container className="mt-5 p-2">
						<Col> 
							<Card className=" mt-2 mt-sm-4">
									<Card.Body className="text-center text-xl-left p-0 d-sm-flex align-items-middle shadow1">

										<div className="container-fluid p-0 product-view-img col-12 col-sm-5">
											<Image  src={productImage} fluid className="w-100 p-0 m-0"/>
										</div>
										<div className="container-fluid p-0 product-view-img col-12 col-sm-7">
											<Nav.Link as={NavLink} to="/products" exact  >
											<h5 className=" text-right pr-4 align-bottom pt-4 go-back "><BiLeftArrowAlt />Go back </h5> </Nav.Link>
											<h1 className="pt-sm-3 mt-sm-3">{product.name}</h1>
											<h4>20 Tea Bags</h4>
											<Card.Text>{product.description}</Card.Text>
											<h2>Php {product.price}</h2>
											<Card.Text className="pr-5 ">Rating {averageRating} </Card.Text>
												<Card.Text className="">Reviews {review}</Card.Text>
										
								
											
											
										</div>
									</Card.Body>
							</Card>
						</Col>
				</Container>

			)	
 }

