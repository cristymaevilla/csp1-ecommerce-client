
import { useState, useEffect, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import { Link, Redirect } from 'react-router-dom';
import { Button, Form }from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../user-context';
 

export default function Login(props){

	const {user, setUser}=useContext(UserContext);

	const[email,setEmail]=useState('');
	const [password , setPassword]=useState('');
	const[isActive, setIsActive]=useState(false);

		function authenticate(e){
			e.preventDefault();
			fetch('http://localhost:4000/users/login', {
			    method: 'POST',
			     headers: {
			         'Content-Type': 'application/json'
			               },
			   body: JSON.stringify({
			         email: email,
			         password: password,
			 
			    })
			})
			.then(res => res.json())
			.then(data => {
				
				if(typeof data.access !== "undefined"){
					localStorage.setItem('token', data.access);
					retrieveUserDetails(data.access);
					
					// console.log();
					Swal.fire({
						title:'Login Successful',
						icon: "success",
						text:`Shop your favorite tea now!`
					})
				}
				else{
					Swal.fire({
						title:"I beg your pardon.",
						icon:"error",
						text:"Check your login details and try again."
					})
				}
			})
			setEmail('');
			setPassword('');
		}

	const retrieveUserDetails = (token) => {
	            fetch('http://localhost:4000/users/details', {
	                headers: {
	                    Authorization: `Bearer ${ token }`
	                }
	            })
	            .then(res => res.json())
	            .then(data => {
	                console.log(data);

	                setUser({
	                    id: data._id,
	                    isAdmin: data.isAdmin,
	                    firstName: data.firstName
	                })
	            })
	        
	}

	useEffect(()=>{
			//enables the submit button
			if(email !== '' && password !== '')
			{setIsActive(true);}
			else{ setIsActive(false)}
		},[email, password])
	
	return( 
		(user.id !== null) ?
		 <Redirect to="/products/all" />
		 :
		<div className="mt-5 pt-5  pb-4  pl-md-5 pr-md-5 vh-100 login">
			<div className="pl-5 pr-5 pt-4 ">
				<h1>Welcome Back your Highness.</h1>
				<Form onSubmit={(e) => authenticate(e)} className="pt-5 align-items-center login-form pl-lg-5 pr-lg-5 col-lg-6">
				  <Form.Group className="mb-3 pl-0  login-form" controlId="formBasicEmail">
				    <Form.Label>Email Address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="" 
				    	className="form-control rounded-0"
				    	value={email}
				    	onChange={(e) => setEmail(e.target.value)}
				    	required  
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-3 pl-0  login-form" controlId="formBasicPassword rounded-0">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="" className="form-control rounded-0"
				    	value={password}
				    	onChange={(e) => setPassword(e.target.value)}
				    	required 
				    	/>
				  </Form.Group>

				  { isActive ? 
				  <Button variant="dark" type="submit" id="submitBtn" className="rounded float-right login-form button-effect">
				    Sign In
				  </Button>
				  :
				  <Button variant="none" type="submit" id="submitBtn" className=" rounded float-right login-form button-effect" disabled>
				    Sign In
				  </Button>
					}
				<div className = ""> Not a Royal yet?
					<Link to="/register"  className="text-register p-0 pl-2" style={{ textDecoration: "none", color: "#A34546", fontWeight:"bold" }}> Register
					</Link>
				</div>
				</Form>
				
				
			</div>
		</div>

)
}
