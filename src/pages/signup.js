import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../user-context.js';

import Image from 'react-bootstrap/Image'
import img from '../images/text.JPG'; 

export default function Register() {

    const {user} = useContext(UserContext);
    const history = useHistory();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [completeAddress, setCompleteAddress] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');

    // Check if values are successfully binded

    // Function to simulate user registration
    function  registerUser(e) {

        e.preventDefault();

        fetch('http://localhost:4000/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log(user.id);
            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch('http://localhost:4000/users/register', {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        completeAddress:completeAddress,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setCompleteAddress('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Zuitt!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        history.push("/login");

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, completeAddress, password1, password2]);
	
	return( 
	(user.id !== null) ?
	<Redirect to="/products" />
	:
	<div className="m-0 pt-5  pb-4  dark-beige  vh-100 ">
		<div className="d-md-flex">
			
			<div className="col-md-6 col-12 container-fluid order-md-2 pt-0 pt-md-5">
			{/*	<Image src={img} classname="vw-100"fluid />*/}
				 <img src="../images/flower3.png" alt="" classname="vw-100 img-fluid"/>
			
			</div>
			<h5> </h5>
			<div className="col-md-6 col-12  order-md-1">
				<Form className="mt-5" onSubmit={(e) => registerUser(e)}>
				  <Form.Group className="mb-2 pl-0" controlId="firstName">
				    <Form.Label>Please fill the form</Form.Label>
				    <Form.Control type="firstName"
				    	 placeholder="First Name" 
				    	 value={firstName}
				    	 onChange={ e => setFirstName(e.target.value)}
				    	 required
				    	 className="form-control rounded-0 placeholder mb-2" />
				  </Form.Group>

				   <Form.Group className="mb-2 pl-0" controlId="lastName">
				
				    <Form.Control type="lastName"
				    	 placeholder="Last Name" 
				    	 value={lastName}
				    	 onChange={ e => setLastName(e.target.value)}
				    	 required
				    	 className="form-control rounded-0 placeholder mb-2" />
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="userEmail">
			
				    <Form.Control type="email" 
				    	placeholder="Email" 
				    	value={email}
				    	onChange = { e => setEmail(e.target.value)}
				    	required 
				    	className="form-control rounded-0 placeholder mb-2"  />
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="mobileNo">
		
				    <Form.Control type="text"  
				    	placeholder="Mobile Number" 
				    	value={mobileNo}
				    	onChange={ e => setMobileNo(e.target.value)}
				    	required 
				    	className="form-control rounded-0  placeholder mb-2" />
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="completeAddress">
				  <Form.Control type="completeAddress"  
				    	placeholder="Complete Address" 
				    	value={completeAddress}
				    	onChange={ e => setCompleteAddress(e.target.value)}
				    	required 
				    	className="form-control rounded-0  placeholder mb-2" />
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="password1">
					   <Form.Control type="password" 
				    	placeholder="Password" 
				    	value={password1}
				    	onChange={ e => setPassword1(e.target.value)}
				    	required
				    	className="form-control rounded-0 placeholder mb-2" />
				  </Form.Group>

				    <Form.Group className="mb-2 pl-0" controlId="password2">
				    <Form.Control type="password" 
				    	placeholder="Verify Password" 
				    	value={password2}
				    	onChange={ e => setPassword2(e.target.value)}
				    	required
				    	className="form-control rounded-0 placeholder mb-2" />
				  </Form.Group>
				  { isActive ? 
				  	<Button variant="primary" type="submit" id="submitBtn" className="rounded-0 float-right" >
				  	    Sign Up
				  	</Button>
				  	:
				  	<Button variant="primary" type="submit" id="submitBtn" className="rounded-0 float-right" disabled>
				  	  Sign Up
				  	</Button>
				   }

				</Form>
			</div>

		</div>
	</div>




		)
}


